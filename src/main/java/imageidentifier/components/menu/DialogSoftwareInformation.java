package imageidentifier.components.menu;

import imageidentifier.components.general.SoftwareInformationPanel;
import imageidentifier.util.ImageIconLoader;
import imageidentifier.util.ImageIdentifierUtils;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;

public class DialogSoftwareInformation extends JDialog {

    public DialogSoftwareInformation() {
        this.setLayout(new MigLayout("ins 0, gap 0, fill, wrap 2", "center", "center"));
        this.setSize(400, 300);

        JPanel appLogo = new JPanel();
        appLogo.add(new JLabel(ImageIconLoader.getIcon(ImageIconLoader.IMAGE_LOGO, 200, 50)));
        appLogo.setPreferredSize(new Dimension(200, 50));
        this.getContentPane().add(appLogo, "span 2, grow");

        JPanel versionTag = new SoftwareInformationPanel(SoftwareInformationPanel.STYLE_TAG, "Version");
        this.getContentPane().add(versionTag, "span 1, grow");

        JPanel versionText = new SoftwareInformationPanel(SoftwareInformationPanel.STYLE_TEXT, ImageIdentifierUtils.getProperty("imageidentifier.version"));
        this.getContentPane().add(versionText, "span 1, grow");

        JPanel patchTag = new SoftwareInformationPanel(SoftwareInformationPanel.STYLE_TAG, "Last Version released on");
        this.getContentPane().add(patchTag, "span 1, grow");

        JPanel patchText = new SoftwareInformationPanel(SoftwareInformationPanel.STYLE_TEXT, ImageIdentifierUtils.getProperty("imageidentifier.lastChanged"));
        this.getContentPane().add(patchText, "span 1, grow");

        JPanel authorTag = new SoftwareInformationPanel(SoftwareInformationPanel.STYLE_TAG, "Author");
        this.getContentPane().add(authorTag, "span 1, grow");

        JPanel authorText = new SoftwareInformationPanel(SoftwareInformationPanel.STYLE_TEXT, "Murad Tochiev");
        this.getContentPane().add(authorText, "span 1, grow");

        this.setTitle("Software information");
        this.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        this.setLocationRelativeTo(this.getParent());
        this.setVisible(true);
    }
}
