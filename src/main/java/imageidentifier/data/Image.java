package imageidentifier.data;

import imageidentifier.util.ImageIconLoader;
import imageidentifier.util.ImageIdentifierUtils;
import org.apache.commons.io.FilenameUtils;
import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Image {
    private final long id;
    private final String filePath;
    private final String fileName;
    private final String parentFolder;
    private final String extension;
    private final long fileSizeAsBytes;
    private final String fileSizeAsText;
    private ImageIcon thumbnail;
    private ImageIcon preview;
    private int width;
    private int height;

    public Image(File file) {
        this.id = System.nanoTime();
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            this.width = bufferedImage.getWidth();
            this.height = bufferedImage.getHeight();
            this.thumbnail = new ImageIcon(ImageIconLoader.resize(bufferedImage, 50, 50));
            this.preview = generateDetailsImage(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.filePath = file.getAbsolutePath();
        this.fileName = file.getName();
        this.parentFolder = file.getParent();
        this.extension = FilenameUtils.getExtension(file.getName());
        this.fileSizeAsBytes = file.length();
        this.fileSizeAsText = ImageIdentifierUtils.convertFileSizeToText(fileSizeAsBytes);
    }

    private ImageIcon generateDetailsImage(File file) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(file);

        int newWidth;
        int newHeight;

        if (this.width < this.height) {
            newWidth = 300;
            newHeight = (int) Math.ceil(((double) newWidth * this.height) / this.width);
        } else {
            newHeight = 300;
            newWidth = (int) Math.ceil(((double) newHeight * this.width) / this.height);
        }
        BufferedImage resizedImage = Scalr.resize(bufferedImage, newWidth, newHeight);
        int cropStartX = (int) Math.ceil(((double) resizedImage.getWidth() / 2) - ((double) 300 / 2));
        int cropStartY = (int) Math.ceil(((double) resizedImage.getHeight() / 2) - ((double) 300 / 2));
        return new ImageIcon(Scalr.crop(resizedImage, cropStartX, cropStartY, 300, 300));
    }

    public ImageIcon getPreview() {
        return preview;
    }

    public long getId() {
        return id;
    }

    public String getFileSizeAsText() {
        return fileSizeAsText;
    }

    public String getParentFolder() {
        return parentFolder;
    }

    public String getFilePath() {
        return filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public String getExtension() {
        return extension;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public ImageIcon getThumbnail() {
        return thumbnail;
    }

}
