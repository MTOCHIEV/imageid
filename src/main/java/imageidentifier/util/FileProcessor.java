package imageidentifier.util;

import imageidentifier.components.ApplicationWindow;
import imageidentifier.components.ErrorTextPanel;
import imageidentifier.components.MainImagePanel;
import imageidentifier.data.AllowedImageTypes;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class FileProcessor {

    private static List<File> fileList;

    public static void addFile(File file) throws IOException {
        if (fileList == null) {
            fileList = new ArrayList<>();
        }
        if (file == null) {
            ErrorTextPanel component = (ErrorTextPanel) ApplicationWindow.getComponentByName("errorTextPanel");
            component.setText("Could not read an image (file is empty)", ImageIdentifierUtils.STATUS_ERROR);
            return;
        }
        if (!isFileAllowed(file)) {
            ErrorTextPanel component = (ErrorTextPanel) ApplicationWindow.getComponentByName("errorTextPanel");
            component.setText("NOT SUPPORTED FILE TYPE: " + file.getAbsolutePath(), ImageIdentifierUtils.STATUS_ERROR);
            return;
        }

        MainImagePanel component = (MainImagePanel) ApplicationWindow.getComponentByName("mainImagePanelScrollPane");
        component.addImage(file);
    }

    public static boolean isFileAllowed(File file) throws IOException {
        String fileMimeType = Files.probeContentType(file.toPath());
        return EnumSet.allOf(AllowedImageTypes.class).stream().anyMatch(o -> fileMimeType.equalsIgnoreCase(o.getMimeType()));
    }
}
