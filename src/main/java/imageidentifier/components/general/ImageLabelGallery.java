package imageidentifier.components.general;

import imageidentifier.components.*;
import imageidentifier.data.Image;
import imageidentifier.util.ImageIdentifierColors;
import imageidentifier.util.ImageIdentifierUtils;
import lombok.Data;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

@Data
public class ImageLabelGallery extends JPanel implements MouseListener {

    Image image;

    public ImageLabelGallery(Image image) {
        this.image = image;
        this.setLayout(new MigLayout("ins 0, gap 0, wrap 1", "center", "top"));
        JLabel thumbnail = new JLabel(image.getThumbnail());
        thumbnail.addMouseListener(this);
        JTextArea fileNameTextArea = new JTextArea();
        fileNameTextArea.setText(image.getFileName().substring(0, Math.min(18, image.getFileName().length())));
        fileNameTextArea.setText(StringUtils.rightPad(fileNameTextArea.getText(), 20, " "));
        fileNameTextArea.setWrapStyleWord(true);
        fileNameTextArea.setLineWrap(true);
        fileNameTextArea.setEditable(false);
        fileNameTextArea.setFont(ImageIdentifierUtils.tahomaPlain10);
        fileNameTextArea.setPreferredSize(new Dimension(50, 20));
        for (MouseListener mouseListener : fileNameTextArea.getMouseListeners()) {
            fileNameTextArea.removeMouseListener(mouseListener);
        }
        fileNameTextArea.addMouseListener(this);
        this.addMouseListener(this);
        this.setPreferredSize(new Dimension(60, 80));
        this.add(thumbnail);
        this.add(fileNameTextArea);
        this.setVisible(true);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getButton() == 1) {
            MainImagePanel scrollContainer = (MainImagePanel) ApplicationWindow.getComponentByName("mainImagePanelScrollPane");
            JPanel mainImagePanelGallery = scrollContainer.getMainImagePanelGallery();
            for (Component component : mainImagePanelGallery.getComponents()) {
                for (Component c : ((ImageLabelGallery) component).getComponents()) {
                    c.setBackground(null);
                }
            }
            for (Component component : this.getComponents()) {
                component.setBackground(ImageIdentifierColors.BLUE_2);
            }
            ImagePreviewPanel imagePreviewPanel = (ImagePreviewPanel) ApplicationWindow.getComponentByName("imagePreviewPanel");
            imagePreviewPanel.loadImage(image);
            ImageDetailsPanel imageDetailsPanel = (ImageDetailsPanel) ApplicationWindow.getComponentByName("imageDetailsPanel");
            imageDetailsPanel.addImageDetails(image);
            IdentifyButtonPanel identifyButtonPanel = (IdentifyButtonPanel) ApplicationWindow.getComponentByName("identifyButtonPanel");
            identifyButtonPanel.setIdentifyButtonSelected(true);
            identifyButtonPanel.setSelectedImage(image);
            ExportPanel exportPanel = (ExportPanel) ApplicationWindow.getComponentByName("exportPanel");
            exportPanel.setAllStatus(false);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
