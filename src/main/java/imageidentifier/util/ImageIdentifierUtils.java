package imageidentifier.util;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ImageIdentifierUtils {

    public static Properties properties;
    public static String STATUS_NOTIFICATION;
    public static final Font tahomaBold16 = new Font("Tahoma", Font.BOLD, 16);
    public static final Font tahomaPlain16 = new Font("Tahoma", Font.PLAIN, 16);
    public static final Font tahomaBold12 = new Font("Tahoma", Font.BOLD, 12);
    public static final Font tahomaPlain12 = new Font("Tahoma", Font.PLAIN, 12);
    public static final Font tahomaBold10 = new Font("Tahoma", Font.BOLD, 10);
    public static final Font tahomaPlain10 = new Font("Tahoma", Font.PLAIN, 10);
    public static final Color STATUS_INFO = new Color(1, 1, 1);
    public static final Color STATUS_ERROR = new Color(212, 57, 0);
    public static int screenWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
    public static int screenHeight = Toolkit.getDefaultToolkit().getScreenSize().height;

    public static void loadProperties() {
        properties = new Properties();
        try {
            InputStream inputStream = ImageIdentifierUtils.class.getResourceAsStream("/application.properties");
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getProperty(String key) {
        return properties.getProperty(key);
    }

    public static String convertFileSizeToText(long fileSizeInByte) {
        long kilo = 1024;
        long mega = kilo * kilo;
        long giga = mega * kilo;
        long tera = giga * kilo;

        String s = "";
        double kb = (double) fileSizeInByte / kilo;
        double mb = kb / kilo;
        double gb = mb / kilo;
        double tb = gb / kilo;
        if (fileSizeInByte < kilo) {
            s = fileSizeInByte + " Bytes";
        } else if (fileSizeInByte >= kilo && fileSizeInByte < mega) {
            s = String.format("%.2f", kb) + " KB";
        } else if (fileSizeInByte >= mega && fileSizeInByte < giga) {
            s = String.format("%.2f", mb) + " MB";
        } else if (fileSizeInByte >= giga && fileSizeInByte < tera) {
            s = String.format("%.2f", gb) + " GB";
        } else if (fileSizeInByte >= tera) {
            s = String.format("%.2f", tb) + " TB";
        }
        return s;
    }

}
