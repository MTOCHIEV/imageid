package imageidentifier.components.menu;

import imageidentifier.util.ImageIdentifierColors;

import javax.swing.*;

public class MenuBar extends JMenuBar {

    public MenuBar() {
        JLabel logo = new MenuLogo();
        JMenu menuFile = new MenuFile();
        JMenu menuOptions = new MenuOptions();
        JMenu menuView = new MenuView();
        JMenu menuHelp = new MenuHelp();

        this.setName("menuBar");
        this.add(Box.createHorizontalStrut(5));
        this.add(logo);
        this.add(Box.createHorizontalStrut(5));
        this.add(menuFile);
        this.add(menuOptions);
        this.add(menuView);
        this.add(menuHelp);
        this.setOpaque(true);
        this.setBackground(ImageIdentifierColors.BLUE_3);

    }
}
