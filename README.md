# ImageIdentififer

## About
ImageIdentifier is a Java Swing Desktop Application to tag images with labels, landmarks and content warnings.
Image analysis is provided by Google Cloud Vision API.

![The interface](/src/main/resources/images/interface.png "Interface"){: .shadow}

* Images can be displayed as a gallery or list view

* The image preview and image details pane can be hidden.

* Supported formats
    * JPG, PNG, WEBP, GIF, PDF, BMP

* Images can be loaded via drag&drop
