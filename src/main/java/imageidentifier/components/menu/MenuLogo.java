package imageidentifier.components.menu;

import imageidentifier.util.ImageIconLoader;

import javax.swing.*;

public class MenuLogo extends JLabel {

    public MenuLogo() {
        this.setIcon(ImageIconLoader.getIcon(ImageIconLoader.IMAGE_LOGOICONONLY, 25, 16));
    }
}
