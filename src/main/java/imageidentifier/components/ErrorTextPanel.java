package imageidentifier.components;

import com.formdev.flatlaf.util.StringUtils;
import imageidentifier.util.ImageIconLoader;
import imageidentifier.util.ImageIdentifierColors;
import imageidentifier.util.ImageIdentifierUtils;

import javax.swing.*;
import java.awt.*;

public class ErrorTextPanel extends JPanel {

    private final JTextArea textArea;
    private final JButton closeButton;

    public ErrorTextPanel() {
        this.setName("errorTextPanel");
        textArea = new JTextArea();
        textArea.setText(ImageIdentifierUtils.STATUS_NOTIFICATION);
        textArea.setOpaque(false);
        textArea.setFont(ImageIdentifierUtils.tahomaBold12);
        textArea.setEditable(false);
        closeButton = new JButton();
        closeButton.setIcon(ImageIconLoader.getIcon(ImageIconLoader.ICON_CANCEL));
        closeButton.setBorderPainted(false);
        closeButton.setBorder(null);
        closeButton.setBackground(ImageIdentifierColors.BLUE_4);
        closeButton.addActionListener(e -> {
            this.setVisible(false);
            textArea.setText(null);
        });
        this.add(closeButton);
        this.add(textArea);
        this.setPreferredSize(new Dimension(ImageIdentifierUtils.screenWidth, ImageIdentifierUtils.screenHeight / 16 * 1));
        this.setOpaque(false);
        this.setVisible(false);
    }

    public void setText(String text, Color color) {
        textArea.setText(text);
        textArea.setForeground(color);
        closeButton.setVisible(!StringUtils.isEmpty(text));
        this.setVisible(!StringUtils.isEmpty(text));
    }
}
