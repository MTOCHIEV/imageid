package imageidentifier.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "imageOutputList")
@XmlAccessorType(XmlAccessType.FIELD)
public class ImageOutputList {

    @XmlElement(name = "imageOutput")
    private List<ImageOutput> imageOutputList;

    public ImageOutputList() {
    }

    public List<ImageOutput> getImageOutputList() {
        return imageOutputList;
    }

    public void setImageOutputList(List<ImageOutput> imageOutputList) {
        this.imageOutputList = imageOutputList;
    }
}