package imageidentifier.util;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.net.URL;

public class ImageIconLoader {

    public static final String ICON_DOCUMENT = "/icons/icon_document.png";
    public static final String ICON_FOLDER = "/icons/icon_folder.png";
    public static final String ICON_GITLAB = "/icons/icon_gitlab.png";
    public static final String ICON_INFO = "/icons/icon_info.png";
    public static final String ICON_INTERNET = "/icons/icon_internet.png";
    public static final String ICON_KEYBOARD = "/icons/icon_keyboard.png";
    public static final String ICON_CANCEL = "/icons/icon_cancel.png";
    public static final String IMAGE_LOGO = "/images/logo.png";
    public static final String IMAGE_LOGOICONONLY = "/images/logo_iconOnly.png";
    public static final String IMAGE_THUMBNAIL = "/images/image_thumbnail.png";

    public static ImageIcon getIcon(String icon) {
        return getIcon(icon, 16, 16);
    }

    public static ImageIcon getIcon(String icon, int width, int height) {
        URL iconURL = ImageIconLoader.class.getResource(icon);
        ImageIcon imageIcon = new ImageIcon(iconURL);
        Image image = imageIcon.getImage();
        Image scaledImage = image.getScaledInstance(width, height, java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(scaledImage);
    }

    public static BufferedImage resize(BufferedImage img, int newW, int newH) {
        Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
        BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = dimg.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        return dimg;
    }
}
