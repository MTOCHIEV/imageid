package imageidentifier.components;

import imageidentifier.components.general.StyledButton;
import imageidentifier.components.menu.DialogTagPreview;
import imageidentifier.util.ImageIdentifierColors;
import imageidentifier.util.ImageIdentifierUtils;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang3.SystemUtils;

import javax.swing.*;
import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;

public class ExportPanel extends JPanel {

    private final StyledButton exportPanelButtonXMLPreview;
    private final StyledButton exportPanelButtonXMLExport;
    private final StyledButton exportPanelButtonJSONPreview;
    private final StyledButton exportPanelButtonJSONExport;
    private final StyledButton exportPanelButtonCSVPreview;
    private final StyledButton exportPanelButtonCSVExport;
    private String xml;
    private String json;
    private String csv;

    public ExportPanel() {
        this.setName("exportPanel");
        this.setLayout(new MigLayout("ins 0, gap 0, wrap 2, fill", "center", "center"));

        exportPanelButtonXMLPreview = new StyledButton("Preview XML");
        exportPanelButtonXMLExport = new StyledButton("Export XML");
        exportPanelButtonJSONPreview = new StyledButton("Preview JSON");
        exportPanelButtonJSONExport = new StyledButton("Export JSON");
        exportPanelButtonCSVPreview = new StyledButton("Preview CSV");
        exportPanelButtonCSVExport = new StyledButton("Export CSV");
        setAllStatus(false);

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(SystemUtils.getUserHome());

        exportPanelButtonXMLExport.addActionListener(e -> {
            fileChooser.showSaveDialog(null);
            try {
                FileWriter fileWriter = new FileWriter(fileChooser.getSelectedFile() + ".xml");
                fileWriter.write(xml);
                fileWriter.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        exportPanelButtonJSONExport.addActionListener(e -> {
            fileChooser.showSaveDialog(null);
            try {
                FileWriter fileWriter = new FileWriter(fileChooser.getSelectedFile() + ".json");
                fileWriter.write(json);
                fileWriter.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        exportPanelButtonCSVExport.addActionListener(e -> {
            fileChooser.showSaveDialog(null);
            try {
                FileWriter fileWriter = new FileWriter(fileChooser.getSelectedFile() + ".csv");
                fileWriter.write(csv);
                fileWriter.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        exportPanelButtonXMLPreview.addActionListener(e -> {
            new DialogTagPreview(xml);
        });
        exportPanelButtonJSONPreview.addActionListener(e -> {
            new DialogTagPreview(json);
        });
        exportPanelButtonCSVPreview.addActionListener(e -> {
            new DialogTagPreview(csv);
        });
        this.add(exportPanelButtonXMLPreview, "span 1, grow");
        this.add(exportPanelButtonXMLExport, "span 1, grow");
        this.add(exportPanelButtonJSONPreview, "span 1, grow");
        this.add(exportPanelButtonJSONExport, "span 1, grow");
        this.add(exportPanelButtonCSVPreview, "span 1, grow");
        this.add(exportPanelButtonCSVExport, "span 1, grow");
        this.setPreferredSize(new Dimension(ImageIdentifierUtils.screenWidth / 5 * 2, ImageIdentifierUtils.screenHeight));
        this.setBackground(ImageIdentifierColors.BLUE_2);
        this.setVisible(true);
    }

    public void setAllStatus(boolean status) {
        exportPanelButtonXMLPreview.setEnabled(status);
        exportPanelButtonXMLExport.setEnabled(status);
        exportPanelButtonJSONPreview.setEnabled(status);
        exportPanelButtonJSONExport.setEnabled(status);
        exportPanelButtonCSVPreview.setEnabled(status);
        exportPanelButtonCSVExport.setEnabled(status);
    }

    public void activateButtons(String xml, String json, String csv) {
        this.xml = xml;
        this.json = json;
        this.csv = csv;
        setAllStatus(true);
    }
}
