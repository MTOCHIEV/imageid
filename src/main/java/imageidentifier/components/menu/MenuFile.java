package imageidentifier.components.menu;

import imageidentifier.util.ImageIconLoader;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

public class MenuFile extends JMenu {

    public MenuFile() {
        this.setText("File");
        JMenuItem menuFileLoadFS = new JMenuItem("Load files from file system");
        menuFileLoadFS.setIcon(ImageIconLoader.getIcon(ImageIconLoader.ICON_FOLDER));
        menuFileLoadFS.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser(FileSystemView.getFileSystemView());
            FileNameExtensionFilter fileNameExtensionFilter = new FileNameExtensionFilter("Only images", "jpg", "png");
            fileChooser.setMultiSelectionEnabled(true);
            fileChooser.setFileFilter(fileNameExtensionFilter);
            fileChooser.setDialogTitle("Choose images");
            fileChooser.showOpenDialog(null);
        });
        JMenuItem menuFileLoadInternet = new JMenuItem("Load file from URL");
        menuFileLoadInternet.setIcon(ImageIconLoader.getIcon(ImageIconLoader.ICON_INTERNET));
        this.add(menuFileLoadFS);
        this.add(menuFileLoadInternet);
    }
}
