package imageidentifier.util;

import java.awt.*;

public class ImageIdentifierColors {

    public static final Color BLUE_1 = new Color(216, 225, 232);
    public static final Color BLUE_2 = new Color(178, 203, 222);
    public static final Color BLUE_3 = new Color(198, 211, 227);
    public static final Color BLUE_4 = new Color(152, 186, 213);
    public static final Color BLUE_5 = new Color(48, 70, 116);

}
