package imageidentifier.components;

import com.formdev.flatlaf.FlatLightLaf;
import imageidentifier.components.menu.MenuBar;
import imageidentifier.util.ImageIdentifierColors;
import lombok.Data;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

@Data
public class ApplicationWindow {

    private static JFrame mainWindow;
    private static HashMap<String, Component> componentMap = new HashMap<>();

    public ApplicationWindow() {
        setup();

        MigLayout migLayout = new MigLayout("ins 0, gap 0, filly,  hidemode 3, wrap 5", "center", "center");
        mainWindow.getContentPane().setLayout(migLayout);
        mainWindow.setJMenuBar(new MenuBar());
        mainWindow.getContentPane().add(new MainImagePanel(), "cell 0 0 9 15, grow");
        mainWindow.getContentPane().add(new ImagePreviewPanel(), "cell 10 0 7 6, grow");
        mainWindow.getContentPane().add(new IdentifyButtonPanel(), "cell 10 6 6 2, grow");
        mainWindow.getContentPane().add(new ImageDetailsPanel(), "cell 10 8 7 4, grow");
        mainWindow.getContentPane().add(new ExportPanel(), "cell 10 12 7 3, grow");
        mainWindow.getContentPane().add(new ErrorTextPanel(), "cell 0 15 16 1");
        createComponentMap(mainWindow);
    }

    private static void createComponentMap(Container container) {
        for (Component component : container.getComponents()) {
            if (component instanceof Container) {
                componentMap.put(component.getName(), component);
                createComponentMap((Container) component);
            } else {
                componentMap.put(component.getName(), component);
            }
        }
    }

    public static Component getComponentByName(String name) {
        if (componentMap.containsKey(name)) {
            return componentMap.get(name);
        } else return null;
    }

    private void setup() {
        try {
            UIManager.setLookAndFeel(new FlatLightLaf());
        } catch (Exception ex) {
            System.err.println("Failed to initialize theme. Using fallback.");
        }
        mainWindow = new JFrame();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        mainWindow.setTitle("Image Identifier");
        mainWindow.getContentPane().setBackground(ImageIdentifierColors.BLUE_4);
        mainWindow.setSize(new Dimension(1000, 800));
        mainWindow.setLocation(screenSize.width / 2 - mainWindow.getSize().width / 2, screenSize.height / 2 - mainWindow.getSize().height / 2);
        mainWindow.setLocationRelativeTo(null);
        mainWindow.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public void run() {
        mainWindow.setVisible(true);
    }
}