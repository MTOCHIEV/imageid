package imageidentifier.components.general;

import imageidentifier.util.ImageIdentifierColors;
import imageidentifier.util.ImageIdentifierUtils;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class SoftwareInformationPanel extends JPanel {

    public static final String STYLE_TAG = "TAG";
    public static final String STYLE_TEXT = "TEXT";

    public SoftwareInformationPanel(String style, String text) {
        this.setLayout(new BorderLayout());
        JTextArea tagText = new JTextArea();
        tagText.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        tagText.setWrapStyleWord(false);
        tagText.setLineWrap(true);
        tagText.setEditable(false);
        tagText.setText(text);

        switch (style) {
            case STYLE_TAG -> {
                tagText.setFont(ImageIdentifierUtils.tahomaBold12);
                tagText.setForeground(Color.WHITE);
                tagText.setBackground(ImageIdentifierColors.BLUE_5);
                this.setBackground(ImageIdentifierColors.BLUE_5);
            }
            case STYLE_TEXT -> {
                tagText.setFont(ImageIdentifierUtils.tahomaBold12);
                tagText.setForeground(Color.BLACK);
                tagText.setBackground(ImageIdentifierColors.BLUE_3);
                this.setBackground(ImageIdentifierColors.BLUE_3);
            }
        }

        this.add(tagText, BorderLayout.CENTER);
        this.setBorder(new LineBorder(ImageIdentifierColors.BLUE_3, 3));
    }
}
