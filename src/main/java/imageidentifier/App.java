package imageidentifier;

import imageidentifier.components.ApplicationWindow;
import imageidentifier.util.ImageIdentifierUtils;

public class App {

    public static void main(String[] args) {
        ImageIdentifierUtils.loadProperties();
        ApplicationWindow mainWindow = new ApplicationWindow();
        mainWindow.run();
    }
}
