package imageidentifier.components.menu;

import imageidentifier.components.ApplicationWindow;
import imageidentifier.components.ErrorTextPanel;
import imageidentifier.util.ImageIconLoader;
import imageidentifier.util.ImageIdentifierUtils;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class MenuHelp extends JMenu {

    public MenuHelp() {
        this.setText("Help");
        JMenuItem menuHelpDocumentation = new JMenuItem("Documentation");
        JMenuItem menuHelpSoftwareInfo = new JMenuItem("Software information");
        JMenuItem menuHelpGitLab = new JMenuItem("GitLab repository");
        JMenuItem menuHelpKeyboardShortcuts = new JMenuItem("Keyboard shortcuts");
        menuHelpDocumentation.setIcon(ImageIconLoader.getIcon(ImageIconLoader.ICON_DOCUMENT));
        menuHelpSoftwareInfo.setIcon(ImageIconLoader.getIcon(ImageIconLoader.ICON_INFO));
        menuHelpGitLab.setIcon(ImageIconLoader.getIcon(ImageIconLoader.ICON_GITLAB));
        menuHelpKeyboardShortcuts.setIcon(ImageIconLoader.getIcon(ImageIconLoader.ICON_KEYBOARD));
        menuHelpDocumentation.addActionListener(e -> {
            File documentation = new File("docs/sample.pdf");
            try {
                Desktop.getDesktop().open(documentation);
            } catch (IOException ex) {
                ex.printStackTrace();
                ErrorTextPanel component = (ErrorTextPanel) ApplicationWindow.getComponentByName("errorTextPanel");
                component.setText("COULD NOT OPEN DOCUMENTATION PDF FILE", ImageIdentifierUtils.STATUS_ERROR);
                return;
            }
        });
        menuHelpSoftwareInfo.addActionListener(e -> {
            new DialogSoftwareInformation();
        });
        menuHelpGitLab.addActionListener(e -> {
            try {
                Desktop.getDesktop().browse(new URI("https://gitlab.com/MTOCHIEV/imageid"));
            } catch (IOException | URISyntaxException exception) {
                exception.printStackTrace();
                ErrorTextPanel component = (ErrorTextPanel) ApplicationWindow.getComponentByName("errorTextPanel");
                component.setText("COULD NOT OPEN BROWSER", ImageIdentifierUtils.STATUS_ERROR);
                return;
            }
        });
        menuHelpKeyboardShortcuts.addActionListener(e -> {
            ErrorTextPanel component = (ErrorTextPanel) ApplicationWindow.getComponentByName("errorTextPanel");
            component.setText("NEW ERROR TEXT", ImageIdentifierUtils.STATUS_ERROR);
        });

        this.add(menuHelpDocumentation);
        this.add(menuHelpGitLab);
        this.add(menuHelpSoftwareInfo);
        this.add(menuHelpKeyboardShortcuts);
    }
}
