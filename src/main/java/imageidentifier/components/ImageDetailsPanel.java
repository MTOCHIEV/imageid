package imageidentifier.components;

import imageidentifier.components.general.SoftwareInformationPanel;
import imageidentifier.data.Image;
import imageidentifier.util.ImageIdentifierColors;
import imageidentifier.util.ImageIdentifierUtils;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;

public class ImageDetailsPanel extends JPanel {

    public ImageDetailsPanel() {
        this.setName("imageDetailsPanel");
        this.setLayout(new MigLayout("hidemode 3, ins 0, gap 0, wrap 2, fill", "center", "center"));
        JLabel imageDetails = new JLabel();
        imageDetails.setText("Image Details");
        imageDetails.setFont(ImageIdentifierUtils.tahomaBold16);
        imageDetails.setForeground(Color.WHITE);
        this.add(imageDetails, "span 2, wrap");
        this.setPreferredSize(new Dimension(ImageIdentifierUtils.screenWidth / 5 * 2, ImageIdentifierUtils.screenHeight));
        this.setBackground(ImageIdentifierColors.BLUE_2);
        this.setVisible(true);
    }

    public void addImageDetails(Image image) {
        this.removeAll();
        JLabel imageDetails = new JLabel();
        imageDetails.setText("Image Details");
        imageDetails.setFont(ImageIdentifierUtils.tahomaBold16);
        imageDetails.setForeground(Color.WHITE);

        SoftwareInformationPanel filenameTag = new SoftwareInformationPanel(SoftwareInformationPanel.STYLE_TAG, "FILE NAME");
        SoftwareInformationPanel filenameText = new SoftwareInformationPanel(SoftwareInformationPanel.STYLE_TEXT, image.getFileName());

        SoftwareInformationPanel filepathTag = new SoftwareInformationPanel(SoftwareInformationPanel.STYLE_TAG, "FILE PATH");
        SoftwareInformationPanel filepathText = new SoftwareInformationPanel(SoftwareInformationPanel.STYLE_TEXT, image.getFilePath());

        SoftwareInformationPanel widthTag = new SoftwareInformationPanel(SoftwareInformationPanel.STYLE_TAG, "WIDTH");
        SoftwareInformationPanel widthText = new SoftwareInformationPanel(SoftwareInformationPanel.STYLE_TEXT, Integer.toString(image.getWidth()));

        SoftwareInformationPanel heightTag = new SoftwareInformationPanel(SoftwareInformationPanel.STYLE_TAG, "HEIGHT");
        SoftwareInformationPanel heightText = new SoftwareInformationPanel(SoftwareInformationPanel.STYLE_TEXT, Integer.toString(image.getHeight()));

        SoftwareInformationPanel fileSizeTag = new SoftwareInformationPanel(SoftwareInformationPanel.STYLE_TAG, "FILE SIZE");
        SoftwareInformationPanel fileSizeText = new SoftwareInformationPanel(SoftwareInformationPanel.STYLE_TEXT, image.getFileSizeAsText());

        this.add(imageDetails, "span 2, wrap");
        this.add(filenameTag, "span 1, grow");
        this.add(filenameText, "span 1, grow");
        this.add(filepathTag, "span 1, grow");
        this.add(filepathText, "span 1, grow");
        this.add(widthTag, "span 1, grow");
        this.add(widthText, "span 1, grow");
        this.add(heightTag, "span 1, grow");
        this.add(heightText, "span 1, grow");
        this.add(fileSizeTag, "span 1, grow");
        this.add(fileSizeText, "span 1, grow");
    }
}
