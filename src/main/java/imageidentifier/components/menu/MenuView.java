package imageidentifier.components.menu;

import imageidentifier.components.ApplicationWindow;
import imageidentifier.components.MainImagePanel;
import lombok.Data;

import javax.swing.*;
import java.awt.*;

@Data
public class MenuView extends JMenu {

    public MenuView() {
        this.setName("menuView");
        this.setText("View");

        JCheckBoxMenuItem menuViewPreview = new JCheckBoxMenuItem("Image View");
        menuViewPreview.setSelected(true);
        menuViewPreview.addActionListener(e -> {
            Component component = ApplicationWindow.getComponentByName("imagePreviewPanel");
            component.setVisible(menuViewPreview.isSelected());
        });

        JCheckBoxMenuItem menuViewDetails = new JCheckBoxMenuItem("Details View");
        menuViewDetails.setSelected(true);
        menuViewDetails.addActionListener(e -> {
            Component component = ApplicationWindow.getComponentByName("imageDetailsPanel");
            component.setVisible(menuViewDetails.isSelected());
        });
        JRadioButtonMenuItem menuViewImageDisplayGallery = new JRadioButtonMenuItem("Display images as gallery", true);
        JRadioButtonMenuItem menuViewImageDisplayList = new JRadioButtonMenuItem("Display images as list", false);
        menuViewImageDisplayGallery.addActionListener(e -> {
            MainImagePanel mainImagePanel = (MainImagePanel) ApplicationWindow.getComponentByName("mainImagePanelScrollPane");
            mainImagePanel.setGalleryView(true);
            mainImagePanel.toggleDisplay();
            menuViewImageDisplayGallery.setSelected(mainImagePanel.isGalleryView());
            menuViewImageDisplayList.setSelected(!mainImagePanel.isGalleryView());
        });
        menuViewImageDisplayList.addActionListener(e -> {
            MainImagePanel mainImagePanel = (MainImagePanel) ApplicationWindow.getComponentByName("mainImagePanelScrollPane");
            mainImagePanel.setGalleryView(false);
            mainImagePanel.toggleDisplay();
            menuViewImageDisplayGallery.setSelected(mainImagePanel.isGalleryView());
            menuViewImageDisplayList.setSelected(!mainImagePanel.isGalleryView());
        });

        this.add(menuViewImageDisplayGallery);
        this.add(menuViewImageDisplayList);
        this.add(menuViewPreview);
        this.add(menuViewDetails);
    }

}
