package imageidentifier.components;

import imageidentifier.data.Image;
import imageidentifier.util.ImageIdentifierColors;
import imageidentifier.util.ImageIdentifierUtils;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;

public class ImagePreviewPanel extends JPanel {

    public ImagePreviewPanel() {
        this.setName("imagePreviewPanel");
        this.setLayout(new MigLayout("ins 0, gap 0, hidemode 3, fill", "center", "center"));

        JLabel defaultMessage = new JLabel("Select an image to show a preview");
        defaultMessage.setHorizontalAlignment(SwingConstants.CENTER);
        defaultMessage.setFont(ImageIdentifierUtils.tahomaBold12);
        defaultMessage.setForeground(Color.WHITE);

        this.add(defaultMessage, "span 2, grow");
        this.setPreferredSize(new Dimension(ImageIdentifierUtils.screenWidth / 5 * 2, ImageIdentifierUtils.screenHeight));
        this.setBackground(ImageIdentifierColors.BLUE_2);
        this.setVisible(true);
    }

    public void setEnabled(boolean isEnabled) {
        this.setVisible(isEnabled);
    }

    public void loadImage(Image image) {
        this.removeAll();
        JLabel imageLabel = new JLabel(image.getPreview());
        imageLabel.setPreferredSize(new Dimension(ImageIdentifierUtils.screenWidth / 5 * 2, ImageIdentifierUtils.screenHeight));
        this.add(imageLabel, "span 1");
        this.revalidate();
        this.repaint();
    }
}
