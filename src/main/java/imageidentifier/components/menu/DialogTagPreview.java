package imageidentifier.components.menu;

import imageidentifier.util.ImageIdentifierUtils;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;

public class DialogTagPreview extends JDialog {

    public DialogTagPreview(String data) {
        this.setLayout(new MigLayout("ins 0, gap 0, fill", "center", "center"));

        JTextArea dataTextArea = new JTextArea();
        dataTextArea.setText(data);
        dataTextArea.setFont(ImageIdentifierUtils.tahomaPlain12);
        dataTextArea.setForeground(Color.BLACK);
        dataTextArea.setBackground(Color.WHITE);
        dataTextArea.setEditable(false);
        dataTextArea.setLineWrap(false);
        dataTextArea.setWrapStyleWord(false);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setAutoscrolls(true);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setViewportView(dataTextArea);
        scrollPane.setVisible(true);

        this.setTitle("Preview");
        this.add(scrollPane, "span 1, grow");
        this.setSize(new Dimension(1200, 1000));
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
}
