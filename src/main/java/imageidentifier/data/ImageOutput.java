package imageidentifier.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "imageOutput")
@XmlAccessorType(XmlAccessType.FIELD)
public class ImageOutput {

    private String filePath;
    private List<String> tags;

    public ImageOutput(String filePath, List<String> tags) {
        this.filePath = filePath;
        this.tags = tags;
    }
    private ImageOutput() {

    }

    public String getFilePath() {
        return filePath;
    }

    public List<String> getTags() {
        return tags;
    }
}
