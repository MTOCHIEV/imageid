package imageidentifier.components.general;

import imageidentifier.util.ImageIdentifierColors;
import imageidentifier.util.ImageIdentifierUtils;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class StyledButton extends JButton {

    public StyledButton(String text) {
        this.setLayout(new BorderLayout());
        this.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        this.setText(text);
        this.setFont(ImageIdentifierUtils.tahomaBold12);
        this.setForeground(Color.WHITE);
        this.setBackground(ImageIdentifierColors.BLUE_5);
        this.setBorder(new LineBorder(ImageIdentifierColors.BLUE_3, 3));
    }
}
