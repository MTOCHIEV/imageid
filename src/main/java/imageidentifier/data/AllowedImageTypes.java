package imageidentifier.data;

public enum AllowedImageTypes {

    PDF("application/pdf"), GIF("image/gif"), TIFF("image/tiff"), JPG("image/jpeg"), PNG("image/png"), BMP("image/bmp"), WEBP("image/webp");

    private final String mimeType;

    AllowedImageTypes(String MIME_TYPE) {
        this.mimeType = MIME_TYPE;
    }

    public String getMimeType() {
        return mimeType;
    }
}
