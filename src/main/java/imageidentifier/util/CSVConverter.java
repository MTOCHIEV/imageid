package imageidentifier.util;

import imageidentifier.data.ImageOutput;
import imageidentifier.data.ImageOutputList;

public class CSVConverter {

    public static String convertTOCSV(ImageOutputList imageOutputList) {

        String csv_separator = ",";
        String line_separator = System.getProperty("line.separator");
        StringBuilder stringBuilder = new StringBuilder();

        //header
        stringBuilder.append("File Path");
        stringBuilder.append(csv_separator);
        stringBuilder.append("Tag");
        stringBuilder.append(line_separator);

        //data
        for (ImageOutput imageOutput : imageOutputList.getImageOutputList()) {
            stringBuilder.append(imageOutput.getFilePath());
            stringBuilder.append(csv_separator);
            for (String tag : imageOutput.getTags()) {
                stringBuilder.append(tag);
                stringBuilder.append(csv_separator);
            }
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            stringBuilder.append(line_separator);
        }
        return stringBuilder.toString();
    }
}
