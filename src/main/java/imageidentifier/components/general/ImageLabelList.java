package imageidentifier.components.general;

import imageidentifier.components.*;
import imageidentifier.data.Image;
import imageidentifier.util.ImageIconLoader;
import imageidentifier.util.ImageIdentifierColors;
import imageidentifier.util.ImageIdentifierUtils;
import lombok.Data;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

@Data
public class ImageLabelList extends JPanel implements MouseListener {

    Image image;

    public ImageLabelList(Image image) {
        this.image = image;
        this.setLayout(new MigLayout("ins 0, gap 0, fillx", "left", "center"));
        this.add(Box.createHorizontalStrut(3), "span 1");
        this.add(new JLabel(ImageIconLoader.getIcon(ImageIconLoader.IMAGE_THUMBNAIL)), "span 1");
        JTextArea fileNameTextArea = new JTextArea();
        String fileNameCutOff = image.getFileName().substring(0, Math.min(55, image.getFileName().length()));
        fileNameTextArea.setText(StringUtils.rightPad(fileNameCutOff, 50, ""));
        fileNameTextArea.setWrapStyleWord(false);
        fileNameTextArea.setLineWrap(false);
        fileNameTextArea.setEditable(false);
        fileNameTextArea.setFont(ImageIdentifierUtils.tahomaPlain10);
        fileNameTextArea.setPreferredSize(new Dimension(350, 20));
        for (MouseListener mouseListener : fileNameTextArea.getMouseListeners()) {
            fileNameTextArea.removeMouseListener(mouseListener);
        }
        fileNameTextArea.addMouseListener(this);
        JTextArea fileType = new JTextArea();
        fileType.setText(StringUtils.rightPad(image.getExtension(), 10, ""));
        fileType.setWrapStyleWord(false);
        fileType.setLineWrap(false);
        fileType.setEditable(false);
        fileType.setFont(ImageIdentifierUtils.tahomaPlain10);
        fileType.setPreferredSize(new Dimension(50, 20));
        for (MouseListener mouseListener : fileType.getMouseListeners()) {
            fileType.removeMouseListener(mouseListener);
        }
        fileType.addMouseListener(this);
        JTextArea parentFolder = new JTextArea();
        parentFolder.setText(StringUtils.rightPad(image.getParentFolder(), 50, ""));
        parentFolder.setWrapStyleWord(false);
        parentFolder.setLineWrap(false);
        parentFolder.setEditable(false);
        parentFolder.setFont(ImageIdentifierUtils.tahomaPlain10);
        parentFolder.setPreferredSize(new Dimension(350, 20));
        for (MouseListener mouseListener : parentFolder.getMouseListeners()) {
            parentFolder.removeMouseListener(mouseListener);
        }
        parentFolder.addMouseListener(this);
        JTextArea fileSize = new JTextArea();
        fileSize.setText(StringUtils.rightPad(image.getFileSizeAsText(), 10, ""));
        fileSize.setWrapStyleWord(false);
        fileSize.setLineWrap(false);
        fileSize.setEditable(false);
        fileSize.setFont(ImageIdentifierUtils.tahomaPlain10);
        fileSize.setPreferredSize(new Dimension(20, 20));
        for (MouseListener mouseListener : fileSize.getMouseListeners()) {
            fileSize.removeMouseListener(mouseListener);
        }
        fileSize.addMouseListener(this);
        this.add(fileNameTextArea, "span 3");
        this.add(fileType, "span 1");
        this.add(parentFolder, "span 3");
        this.add(fileSize, "span 1");
        this.addMouseListener(this);
        this.setVisible(true);
    }

    public ImageLabelList(int columnHeader) {
        this.setLayout(new MigLayout("ins 0, gap 0", "left", "center"));
        this.add(Box.createHorizontalStrut(3), "span 1");
        this.add(new JLabel(ImageIconLoader.getIcon(ImageIconLoader.IMAGE_THUMBNAIL)), "span 1");
        JTextArea fileNameTextArea = new JTextArea();
        fileNameTextArea.setText(StringUtils.rightPad("FILE NAME", 50, ""));
        fileNameTextArea.setWrapStyleWord(false);
        fileNameTextArea.setLineWrap(false);
        fileNameTextArea.setEditable(false);
        fileNameTextArea.setFont(ImageIdentifierUtils.tahomaBold10);
        fileNameTextArea.setPreferredSize(new Dimension(350, 20));
        JTextArea fileType = new JTextArea();
        fileType.setText(StringUtils.rightPad("TYPE", 10, ""));
        fileType.setWrapStyleWord(false);
        fileType.setLineWrap(false);
        fileType.setEditable(false);
        fileType.setFont(ImageIdentifierUtils.tahomaBold10);
        fileType.setPreferredSize(new Dimension(50, 20));
        JTextArea parentFolder = new JTextArea();
        parentFolder.setText(StringUtils.rightPad("IN FOLDER", 50, ""));
        parentFolder.setWrapStyleWord(false);
        parentFolder.setLineWrap(false);
        parentFolder.setEditable(false);
        parentFolder.setFont(ImageIdentifierUtils.tahomaBold10);
        parentFolder.setPreferredSize(new Dimension(350, 20));
        JTextArea fileSize = new JTextArea();
        fileSize.setText(StringUtils.rightPad("SIZE", 10, ""));
        fileSize.setWrapStyleWord(false);
        fileSize.setLineWrap(false);
        fileSize.setEditable(false);
        fileSize.setFont(ImageIdentifierUtils.tahomaBold10);
        fileSize.setPreferredSize(new Dimension(20, 20));
        this.add(fileNameTextArea, "span 3");
        this.add(fileType, "span 1");
        this.add(parentFolder, "span 3");
        this.add(fileSize, "span 1");
        this.addMouseListener(this);
        this.setVisible(true);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getButton() == 1) {
            MainImagePanel scrollContainer = (MainImagePanel) ApplicationWindow.getComponentByName("mainImagePanelScrollPane");
            JPanel mainImagePanelList = scrollContainer.getMainImagePanelList();
            for (Component component : mainImagePanelList.getComponents()) {
                for (Component c : ((ImageLabelList) component).getComponents()) {
                    c.setBackground(null);
                }
            }
            for (Component component : this.getComponents()) {
                component.setBackground(ImageIdentifierColors.BLUE_2);
            }
            ImagePreviewPanel imagePreviewPanel = (ImagePreviewPanel) ApplicationWindow.getComponentByName("imagePreviewPanel");
            imagePreviewPanel.loadImage(image);
            ImageDetailsPanel imageDetailsPanel = (ImageDetailsPanel) ApplicationWindow.getComponentByName("imageDetailsPanel");
            imageDetailsPanel.addImageDetails(image);
            IdentifyButtonPanel identifyButtonPanel = (IdentifyButtonPanel) ApplicationWindow.getComponentByName("identifyButtonPanel");
            identifyButtonPanel.setIdentifyButtonSelected(true);
            identifyButtonPanel.setSelectedImage(image);
            ExportPanel exportPanel = (ExportPanel) ApplicationWindow.getComponentByName("exportPanel");
            exportPanel.setAllStatus(false);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
