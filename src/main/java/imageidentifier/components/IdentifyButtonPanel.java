package imageidentifier.components;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.cloud.vision.v1.*;
import imageidentifier.components.general.StyledButton;
import imageidentifier.data.Image;
import imageidentifier.data.ImageOutput;
import imageidentifier.data.ImageOutputList;
import imageidentifier.util.CSVConverter;
import imageidentifier.util.ImageIdentifierColors;
import imageidentifier.util.ImageIdentifierUtils;
import net.miginfocom.swing.MigLayout;
import org.springframework.cloud.gcp.vision.CloudVisionTemplate;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;

import javax.swing.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.awt.*;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class IdentifyButtonPanel extends JPanel {

    private final StyledButton identifyButtonAll;
    private final StyledButton identifyButtonSelected;
    private final CloudVisionTemplate cloudVisionTemplate;
    private final ResourceLoader resourceLoader;
    private List<Image> images;
    private Image selectedImage;
    private String outputAsXML;
    private String outputAsJSON;
    private String outputAsCSV;

    public IdentifyButtonPanel() {
        this.resourceLoader = new DefaultResourceLoader();
        ImageAnnotatorClient imageAnnot = null;
        try {
            imageAnnot = ImageAnnotatorClient.create();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.cloudVisionTemplate = new CloudVisionTemplate(imageAnnot);

        this.setLayout(new MigLayout("ins 0, gap 0, fill, wrap 2"));
        this.setName("identifyButtonPanel");

        identifyButtonAll = new StyledButton("Identify all images");
        identifyButtonAll.setEnabled(false);
        identifyButtonAll.addActionListener(e -> {
            try {
                identifyAllImages(images);
            } catch (JAXBException | IOException ex) {
                ex.printStackTrace();
            }
        });
        identifyButtonSelected = new StyledButton("Identify selected image");
        identifyButtonSelected.setEnabled(false);
        identifyButtonSelected.addActionListener(e -> {
            try {
                identifySelectedImage(selectedImage);
            } catch (JAXBException | IOException ex) {
                ex.printStackTrace();
            }
        });

        this.add(identifyButtonAll, "span 1, grow");
        this.add(identifyButtonSelected, "span 1, grow");
        this.setPreferredSize(new Dimension(ImageIdentifierUtils.screenWidth / 5 * 2, ImageIdentifierUtils.screenHeight / 16 * 2));
        this.setBackground(ImageIdentifierColors.BLUE_2);
        this.setVisible(true);
    }

    public void identifyAllImages(List<Image> images) throws JAXBException, IOException {
        ImageOutputList imageOutputList = new ImageOutputList();
        imageOutputList.setImageOutputList(new ArrayList<>());
        for (Image image : images) {
            List<String> tags = getImageTagsGoogleCloudVision(image.getFilePath());
            ImageOutput imageOutput = new ImageOutput(image.getFilePath(), tags);
            imageOutputList.getImageOutputList().add(imageOutput);
        }
        // XML
        StringWriter stringWriterXML = new StringWriter();
        JAXBContext jaxbContext = JAXBContext.newInstance(ImageOutputList.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(imageOutputList, stringWriterXML);
        outputAsXML = stringWriterXML.toString();
        //JSON
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        outputAsJSON = ow.writeValueAsString(imageOutputList);
        //CSV
        outputAsCSV = CSVConverter.convertTOCSV(imageOutputList);

        ExportPanel exportPanel = (ExportPanel) ApplicationWindow.getComponentByName("exportPanel");
        exportPanel.activateButtons(outputAsXML, outputAsJSON, outputAsCSV);
    }

    private void identifySelectedImage(Image image) throws JAXBException, JsonProcessingException {
        ImageOutputList imageOutputList = new ImageOutputList();
        imageOutputList.setImageOutputList(new ArrayList<>());
        List<String> tags = getImageTagsGoogleCloudVision(image.getFilePath());
        ImageOutput imageOutput = new ImageOutput(image.getFilePath(), tags);
        imageOutputList.getImageOutputList().add(imageOutput);

        // XML
        StringWriter stringWriterXML = new StringWriter();
        JAXBContext jaxbContext = JAXBContext.newInstance(ImageOutputList.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(imageOutputList, stringWriterXML);
        outputAsXML = stringWriterXML.toString();
        //JSON
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        outputAsJSON = ow.writeValueAsString(imageOutputList);
        //CSV
        outputAsCSV = CSVConverter.convertTOCSV(imageOutputList);
        ExportPanel exportPanel = (ExportPanel) ApplicationWindow.getComponentByName("exportPanel");
        exportPanel.activateButtons(outputAsXML, outputAsJSON, outputAsCSV);
    }

    public List<String> getImageTagsGoogleCloudVision(String filePath) {
        AnnotateImageResponse response = this.cloudVisionTemplate.analyzeImage(this.resourceLoader.getResource("file:" + filePath), Feature.Type.LABEL_DETECTION, Feature.Type.LANDMARK_DETECTION, Feature.Type.LOGO_DETECTION, Feature.Type.SAFE_SEARCH_DETECTION, Feature.Type.WEB_DETECTION);
        List<String> descriptions = new ArrayList<>();

        //Web Detection
        for (WebDetection.WebLabel webLabel : response.getWebDetection().getBestGuessLabelsList()) {
            descriptions.add(webLabel.getLabel());
        }
        //Label Detection
        for (EntityAnnotation entityAnnotation : response.getLabelAnnotationsList()) {
            if (entityAnnotation.getScore() > 0.8f) {
                descriptions.add(entityAnnotation.getDescription());
            }
        }
        //Landmark Detection
        for (EntityAnnotation entityAnnotation : response.getLandmarkAnnotationsList()) {
            if (entityAnnotation.getScore() > 0.8f) {
                descriptions.add(entityAnnotation.getDescription());
            }
        }
        //Logo Detection
        for (EntityAnnotation entityAnnotation : response.getLogoAnnotationsList()) {
            if (entityAnnotation.getScore() > 0.8f) {
                descriptions.add(entityAnnotation.getDescription());
            }
        }

        //SafeSearch
        if (response.getSafeSearchAnnotation().getSpoof() == Likelihood.LIKELY || response.getSafeSearchAnnotation().getSpoof() == Likelihood.VERY_LIKELY) {
            descriptions.add("Meme");
        }
        if (response.getSafeSearchAnnotation().getAdult() == Likelihood.VERY_LIKELY) {
            descriptions.add("Adult Content");
        }
        if (response.getSafeSearchAnnotation().getMedical() == Likelihood.VERY_LIKELY) {
            descriptions.add("Medical Content");
        }
        if (response.getSafeSearchAnnotation().getViolence() == Likelihood.VERY_LIKELY) {
            descriptions.add("Violent Content");
        }
        if (response.getSafeSearchAnnotation().getRacy() == Likelihood.VERY_LIKELY) {
            descriptions.add("Racy Content");
        }


        List<String> descriptionsUnique = new ArrayList<String>(new HashSet<String>(descriptions));
        return descriptionsUnique;
    }

    public String getOutputAsXML() {
        return outputAsXML;
    }

    public String getOutputAsJSON() {
        return outputAsJSON;
    }

    public String getOutputAsCSV() {
        return outputAsCSV;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Image getSelectedImage() {
        return selectedImage;
    }

    public void setSelectedImage(Image selectedImage) {
        this.selectedImage = selectedImage;
    }

    public void setStatusIdentifyAll(boolean isEnabled) {
        identifyButtonAll.setEnabled(isEnabled);
    }

    public void setIdentifyButtonSelected(boolean isEnabled) {
        identifyButtonSelected.setEnabled(isEnabled);
    }
}
