package imageidentifier.components;

import imageidentifier.components.general.ImageLabelGallery;
import imageidentifier.components.general.ImageLabelList;
import imageidentifier.components.general.WrapLayout;
import imageidentifier.data.Image;
import imageidentifier.util.FileProcessor;
import imageidentifier.util.ImageIdentifierColors;
import imageidentifier.util.ImageIdentifierUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainImagePanel extends JScrollPane {

    private final List<Image> images;
    private final JLabel defaultMessage;
    private final JPanel containerPanel;
    private final JPanel mainImagePanelGallery;
    private final JPanel mainImagePanelList;
    private boolean isGalleryView = true;

    public MainImagePanel() {
        this.setName("mainImagePanelScrollPane");

        images = new ArrayList<>();

        defaultMessage = new JLabel("Select files from menu or drag and drop");
        defaultMessage.setHorizontalAlignment(SwingConstants.HORIZONTAL);
        defaultMessage.setFont(ImageIdentifierUtils.tahomaBold12);
        defaultMessage.setForeground(Color.WHITE);

        containerPanel = new JPanel();
        containerPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        containerPanel.setBackground(ImageIdentifierColors.BLUE_5);
        containerPanel.add(defaultMessage);
        containerPanel.setDropTarget(new DropTarget() {
            public synchronized void drop(DropTargetDropEvent evt) {
                try {
                    evt.acceptDrop(DnDConstants.ACTION_COPY);
                    List<File> droppedFiles = (List<File>) evt.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
                    for (File file : droppedFiles) {
                        if (file.isDirectory()) {
                            for (File f : file.listFiles()) {
                                FileProcessor.addFile(f);
                            }
                        } else {
                            FileProcessor.addFile(file);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        mainImagePanelGallery = new JPanel();
        mainImagePanelGallery.setName("mainImagePanelGallery");
        mainImagePanelGallery.setLayout(new WrapLayout(FlowLayout.LEADING));
        mainImagePanelGallery.setBackground(ImageIdentifierColors.BLUE_5);
        mainImagePanelList = new JPanel();
        mainImagePanelList.setName("mainImagePanelList");
        mainImagePanelList.setLayout(new WrapLayout(FlowLayout.LEFT));
        mainImagePanelList.setBackground(ImageIdentifierColors.BLUE_5);

        this.setPreferredSize(new Dimension(ImageIdentifierUtils.screenWidth / 5 * 3, ImageIdentifierUtils.screenHeight));
        this.setAutoscrolls(true);
        this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.setViewportView(containerPanel);
        this.setVisible(true);
    }

    public void addImage(File file) {
        Image newImage = new Image(file);
        images.add(newImage);
        Collections.sort(images, Comparator.comparingLong(Image::getId));
        defaultMessage.setVisible(false);
        IdentifyButtonPanel identifyButtonPanel = (IdentifyButtonPanel) ApplicationWindow.getComponentByName("identifyButtonPanel");
        identifyButtonPanel.setStatusIdentifyAll(true);
        identifyButtonPanel.setImages(images);
        containerPanel.removeAll();
        containerPanel.add(createImageGallery());
        containerPanel.add(createImageList());
        this.revalidate();
        this.repaint();
    }

    private JPanel createImageGallery() {
        mainImagePanelGallery.removeAll();
        for (Image image : images) {
            mainImagePanelGallery.add(new ImageLabelGallery(image));
        }
        if (!isGalleryView) mainImagePanelGallery.setVisible(false);
        return mainImagePanelGallery;
    }


    private JPanel createImageList() {
        mainImagePanelList.removeAll();
        mainImagePanelList.add(new ImageLabelList(0));
        for (Image image : images) {
            mainImagePanelList.add(new ImageLabelList(image));
        }
        if (isGalleryView) mainImagePanelList.setVisible(false);
        return mainImagePanelList;
    }

    public void toggleDisplay() {
        if (isGalleryView) {
            mainImagePanelGallery.setVisible(true);
            mainImagePanelList.setVisible(false);
        } else {
            mainImagePanelGallery.setVisible(false);
            mainImagePanelList.setVisible(true);
        }
    }

    public boolean isGalleryView() {
        return isGalleryView;
    }

    public void setGalleryView(boolean galleryView) {
        isGalleryView = galleryView;
    }

    public JPanel getMainImagePanelGallery() {
        return mainImagePanelGallery;
    }

    public JPanel getMainImagePanelList() {
        return mainImagePanelList;
    }

}